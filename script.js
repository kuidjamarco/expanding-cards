const divImgs = document.querySelectorAll('.img');
divImgs.forEach(divImg => {
    divImg.addEventListener('click', () =>{
        //remove the classe active  div 
        removeClass();

        //on ajoute la classe active au div qu'on viens de cliquer
        divImg.classList.add("active");
    });
});

function removeClass(){
    // const divImgs = document.querySelectorAll('.img');
    divImgs.forEach(divImg => {
        //on enleve la classe active sur image courante
        divImg.classList.remove("active");
    })
}
